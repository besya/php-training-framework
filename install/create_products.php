<?php

require '../vendor/autoload.php';
$config = include '../app/config/dev/db.php';

$capsule = new Illuminate\Database\Capsule\Manager();
$capsule->addConnection($config);
$capsule->setAsGlobal();
$capsule->bootEloquent();

Illuminate\Database\Capsule\Manager::schema()->create('products', function(
        \Illuminate\Database\Schema\Blueprint $table)
{
    $table->increments('id');
    $table->string('name');
    $table->text('description');
    $table->float('price');
});

