<?php

define('ROOT', __DIR__ . '/..');
define('APP', ROOT . '/app');
define('ENV', 'dev');

require ROOT . '/core/autoload.php';
require ROOT . '/vendor/autoload.php';

$config = include APP . '/config/config.php';

$app = new \Core\Application($config);
$app->run();