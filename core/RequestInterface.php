<?php

namespace Core;

interface RequestInterface {
    public function getRequestUri();
}