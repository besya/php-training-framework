<?php

namespace Core;

class Controller
{
    public $app;
    public $view;
    
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    public function render($viewName, $variables){
        $this->app->response->headers['Content-Type'] = 'text/html';
        $result = $this->view->render($viewName, $variables);
        $this->app->response->body[] = $result;
    }
    
    public function renderJson($variables){
        $this->app->response->headers['Content-Type'] = 'application/json';
        $result = $this->view->renderJson($variables);
        $this->app->response->body[] = $result;
    }
    
    public function notFound($message){
        $this->app->response->status = 404;
        $this->app->response->body[] = $message;
    }
}
