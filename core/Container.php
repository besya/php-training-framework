<?php

namespace Core;

class Container {
    private $providers = [];
    private $singles = [];
    private $instances = [];
    
    public function set($name, $callback) {
        $this->providers[$name] = $callback;
    }
    
    public function get($name) {
        if (isset($this->instances[$name])){
            return $this->instances[$name];
        }
        
        if (isset($this->singles[$name])){
            $this->instances[$name] = $this->providers[$name]($this);
            return $this->instances[$name];
        }
        
        return $this->providers[$name]($this);
    }
    
    public function singleton($name, $callback) {
        $this->set($name, $callback);
        $this->singles[$name];
    }
}
