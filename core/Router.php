<?php

namespace Core;

class Router
{
    private $requestUri;
    private $query;
    private $routes = [];
    
    private $isMatched = false;
    
    public $controller;
    public $action;
    public $segments = [];

    public function __construct(RequestInterface $request, $routes)
    {
        $this->routes = $routes;
        $this->requestUri = $request->getRequestUri();
        $this->query = $request->getQueryString();
        $this->run();
    }

    public function isMatched(){
        return $this->isMatched;
    }
    
    private function run(){
        $uri = str_replace('?' . $this->query, '', $this->requestUri);
        $uri = trim($uri, '/');

        foreach ($this->routes as $route => $path){
            if (preg_match("~^$route$~", $uri)){
                $internalPath = preg_replace("~^$route$~", $path, $uri);
                $segments = explode('/', $internalPath);

                $this->isMatched = true;                
                $this->controller = strtolower(array_shift($segments));
                $this->action = strtolower(array_shift($segments));
                $this->segments = $segments;

                return;
            }
        }
    }    
}