<?php

namespace Core;

class View {
    
    public $layoutName = 'shared/layout';
    protected $pageVariables = [];
    
    public function render($viewName, $variables = []){
        extract($variables);
        ob_start();
        $content = $this->renderPartial($viewName, $variables);
        extract($this->pageVariables);
        include APP . '/views/' . $this->layoutName . '.php';
        $page = ob_get_clean();
        return $page;
    }
    
    public function renderPartial($viewName, $variables = []){
        extract($variables);
        ob_start();
        include APP . '/views/' . $viewName . '.php';
        $partial = ob_get_clean();
        return $partial;
    }

    public function renderJson($variables){
        return json_encode($variables);
    }
    
    public function set($varName, $varValue) {
        $this->pageVariables[$varName] = $varValue;
    }
}