<?php

namespace Core;

class Request implements RequestInterface {
    
    private $server = [];
    private $_get = [];
    
    public function __construct() {
        $this->server = $_SERVER;
        $this->_get = $_GET;
    }
    
    public function getRequestUri(){
        return $this->server['REQUEST_URI'];
    }
    
    public function getQueryString(){
        return $this->server['QUERY_STRING'];
    }
    
    public function get($key, $default = ''){
        if(!isset($this->_get[$key])
        || empty($this->_get[$key]) ){
            return $default;
        }
        
        return $this->_get[$key];
    }
    
    public function allGet($fields = []){
        $result = [];
        foreach($fields as $field){
            $result[$field] = $this->get($field);
        }
        return $result;
    }
    
    public function isPost(){
        if($this->server['REQUEST_METHOD'] == 'POST'){
            return true;
        }
        return false;
    }

    public function isDelete(){
        if($this->server['REQUEST_METHOD'] == 'POST'
           && $this->server['X-HTTP-Method'] == 'DELETE'){
            return true;
        }
        return false;
    }
}
