<?php

namespace Core;

use Illuminate\Database\Capsule\Manager as Capsule;

class Application {
    
    public $config = [];
    public $request;
    public $response;
    public $router;
    public $container;
    
    public function __construct($config = []) {
        $this->config = $config;
        $this->config['APP'] = APP;
        $this->config['ROOT'] = ROOT;
        $this->config['ENV'] = ENV;
        $this->container = new Container();
        $this->container->set('config', function ($c) { return $this->config; });
        $container = $this->container;
        include APP . '/config/dependencies.php';
    }
    
    public function run(){
        $this->registerErrorsHandler();
        $this->request = $this->container->get('request');
        $this->response = $this->container->get('response');
        $this->router = $this->container->get('router');
        $this->setDatabaseConnection();
        $this->dispatch();
        $this->response->send();
    }
    
    private function registerErrorsHandler(){
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
        $whoops->register();        
    }
    
    private function dispatch(){
        if(!$this->router->isMatched()){
            throw new \Exception('Not found any routes');
        }
        
        $controllerClass = 
            $this->config['controllersNamespace'] . "\\" .
            ucfirst($this->router->controller . 'Controller');

        $action = 'action' . ucfirst($this->router->action);

        $controller = new $controllerClass($this);
        $controller->view = new View();

        call_user_func_array(
            [$controller, $action],
            $this->router->segments
        );
    }
    
    private function setDatabaseConnection(){
        $capsule = new Capsule;
        $capsule->addConnection($this->config['db']);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}
