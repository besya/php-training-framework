<?php

namespace Core;

class Response {
    
    public $status;
    public $headers = [];
    public $body = [];
    
    private $messages = [
        200 => 'OK',
        404 => 'Not Found'
    ];
    
    public function __construct($status = 200, $headers = [], $body = []) {
        $this->status = $status;
        $this->headers = $headers;
        $this->body = $body;
    }
    
    public function send(){
        header("HTTP/1.1 " . $this->status . " " . $this->messages[$this->status]);
        
        foreach ($this->headers as $headerKey=>$headerValue){
            header($headerKey . ': ' . $headerValue);
        }
        
        foreach ($this->body as $body){
            echo $body;            
        }
    }
}
