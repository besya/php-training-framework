<?php $this->set('title', 'News Index Page'); ?>

<h1>Новости</h1>

<?= $this->renderPartial('widgets/date', ['today' => date('d/m/Y')]) ?>

<?php foreach($news as $item): ?>
    <?= $this->renderPartial('news/_item', compact('item')); ?>    
<?php endforeach; ?>