<html>
    <head>
        <title><?= ($title) ? $title : 'My App'; ?></title>
    </head>
    <body>
        <?= $this->renderPartial('widgets/date', ['today' => date('Y')]) ?>
        <?= $this->renderPartial('shared/header') ?>        
        <div><?= $content; ?></div>
        <footer>Footer</footer>
    </body>    
</html>
