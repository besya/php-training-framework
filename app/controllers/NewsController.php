<?php

namespace App\Controllers;

use App\Models\News;
use App\Models\Product;
use Core\Controller;

class NewsController extends Controller
{    
    public function actionList(){
        echo '<pre>';
        print_r($_SERVER);
        exit();
//        for($i = 1; $i < 11; $i++){
//            $product = new Product();
//            $product->name = 'Product #' . $i;
//            $product->description = 'Product #' . $i . ' description';
//            $product->price = $i;
//            $product->save();
//        }
//                
        $news = Product::orderBy('name', 'ASC')->get();
        $this->render('news/index', compact('news'));
    }

    public function actionView($alias){
        $item = News::where('alias', $alias)->first();
        
        if(!$item){
            $this->notFound('News Item not found');
            return;
        }
        
        $this->render('news/view', compact('item'));
    }
}