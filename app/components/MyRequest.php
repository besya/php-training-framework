<?php

namespace App\Components;

class MyRequest implements \Core\RequestInterface {
    
    private $props = [];
    
    public function __construct() {
        $this->props = $_SERVER;
    }    
    
    public function getRequestUri() {
        return $this->props['REQUEST_URI'];
    }

}
