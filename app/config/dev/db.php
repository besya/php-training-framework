<?php

return [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'custom',
    'username' => 'mysql',
    'password' => 'mysql',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];