<?php

$container->set('request', function ($c) {
    return new \Core\Request();
});

$container->singleton('response', function ($c) {
    return new \Core\Response();
});

$container->singleton('router', function ($c) {
    return new \Core\Router($c->get('request'), $c->get('config')['routes']);
});