<?php

return array_merge(
    [
        'db' => include ENV . '/db.php'
    ],
    [
        // common config
        'routes' => include 'routes.php',
        'controllersNamespace' => 'App\\Controllers'
    ]
);
