<?php

return [
    'news' => 'news/list',
    'news/(.+)' => 'news/view/$1'
];